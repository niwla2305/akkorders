use std::env;

fn main() {
    let args: Vec<_> = env::args().collect();
    if args.len() > 1 {
        for (index, note) in env::args().enumerate() {
            if index == 0 {
                continue;
            }

            let triad = akkorders::get_triad(note);
            let translated_kind;
            if triad.kind == "major" {
                translated_kind = "Dur"
            } else {
                translated_kind = "Moll"
            }
            println!("{}, {}, {} ({})", triad.notes[0], triad.notes[1], triad.notes[2], translated_kind)
        }
    }
}
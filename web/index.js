import * as wasm from "akkorders";

const input = document.getElementById('note_input');

function capitalizeFirstLetter(string) {
  return string.charAt(0).toUpperCase() + string.slice(1)
}

function updateValue(e) {
  if (e.target.value.length > 0) {
    var response = JSON.parse(wasm.get_triad_json(e.target.value));
    document.getElementById("note1").innerHTML = capitalizeFirstLetter(response.notes[0].toString())
    document.getElementById("note2").innerHTML = capitalizeFirstLetter(response.notes[1].toString())
    document.getElementById("note3").innerHTML = capitalizeFirstLetter(response.notes[2].toString())
    document.getElementById("kind").innerHTML = response.kind == "major" ? "Dur" : "Moll"

    document.getElementById("result").style.display = "block"
  } else {
    document.getElementById("result").style.display = "none"
  }

}

input.addEventListener('input', updateValue);




